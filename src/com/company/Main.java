package com.company;

public class Main {

    public static void main(String[] args) {
	    StackOperations stack = new Stack();
	    stack.push("test1");
	    stack.push("test2");
	    System.out.println(stack.get());
	    System.out.println(stack.pop());
		System.out.println(stack.get());
		System.out.println(stack.pop());
		System.out.println(stack.get());
		System.out.println(stack.pop());
    }
}
