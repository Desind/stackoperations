package com.company;

import java.util.*;

public class Stack implements StackOperations {

    private List<String> stackList;

    public Stack() {
        this.stackList = new ArrayList<>();
    }

    @Override
    public List<String> get() {
        if (stackList.size() < 1){
            return null;
        }else{
            List <String> reversedList = new ArrayList<>();
            reversedList.addAll(this.stackList);
            Collections.reverse(reversedList);
            return reversedList;
        }
    }

    @Override
    public Optional<String> pop() {
        if(stackList.size()<1){
            return Optional.empty();
        }else{
            String value = stackList.get(stackList.size()-1);
            stackList.remove(stackList.size()-1);
            return Optional.of(value);
        }
    }

    @Override
    public void push(String item) {
        stackList.add(item);
    }
}
